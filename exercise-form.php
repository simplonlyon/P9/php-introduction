<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercise Form</title>
</head>
<body>
    
<form method="POST">

    <label for="Name">Name:</label>
    <input type="text" name="name">

    <label for="Age">Age:</label>
    <input type="number" name="age">

    <label for="Money">Money:</label>
    <input  type="number" name="money">

    <br>
    <label for="Street">Rue:</label>
    <input type="text" name="street">

    <label for="City">Ville:</label>
    <input type="text" name="city">

    <label for="Country">Pays:</label>
    <input type="text" name="country">
    <button>Submit</button>
</form>

<?php

use App\Exercise\Person;
use App\Exercise\Address;

require_once 'vendor/autoload.php';

$data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

// var_dump($data);
if ($data && is_numeric($data["age"]) && is_numeric($data["money"])) {
    $person = new Person($data["name"], $data["age"], $data["money"]);

    $address = new Address($data["street"], $data["city"], $data["country"]);

    $person->addAddress($address);
    echo $person->listAddress();
}
?>
    
    
</body>
</html>