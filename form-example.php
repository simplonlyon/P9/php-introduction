<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A basic Form</title>
</head>
<body>
    <!-- On fait ici un formulaire qui va envoyer une requête
    HTTP POST vers le fichier form-submit-example.php qui sera
    chargé de traiter les données du formulaire en question -->
    <form action="form-submit-example.php" method="post">
        <!-- Bien penser à mettre le name sur l'input, car la 
        requête en a besoin pour envoyer la donnée -->
        <input type="text" name="test">
        <button>Submit</button>
    </form>


</body>
</html>