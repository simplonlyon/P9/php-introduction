<?php
/**
 * Les namespace sont la manière de PHP pour faire du modulaire.
 * Un truc à l'intérieur d'un namespace n'aura accès qu'aux autres trucs
 * qui sont dans le même namespace.
 * Le standard veut qu'on utilise comme namespace le préfix défini dans
 * le composer.json suivi de l'arboréscence de dossier dans 
 * lequel se trouve le fichier.
 * exemple: un fichier dans le dossier qui est dans src/Folder/SubFolder
 * aura comme namespace App\Folder\SubFolder
 */
namespace App\Example;

/**
 * La définition des classes est la même qu'en JS
 */
class First {
    /**
     * Contrairement au JS, on peut (et il est conseillé de) définir les
     * propriétés d'une classe directement dans la classe hors du constructeur
     * en indiquant la visibilité suivie du nom de la variable.
     * La visibilité permet d'indiqué où une propriété ou une méthode sera
     * accessible (d'où on pourra déclencher une méthode ou récupérer/changer la valeur d'une propriété)
     * Les 3 valeurs possibles sont :
     * public : la prop/méthode est utilisable de n'importe où
     * private : la prop/méthode n'est utilisable que dans les méthodes de la classe en question
     * protected : la prop/méthode n'est utilisable que dans les méthodes de la classe et des classes qui en hérite
     */
    public $prop1;
    private $prop2;
    protected $prop3 = 'test';
    
    /**
     * Le constructeur marche pareil qu'en js sauf qu'il s'appelle
     * __construct au lieu de constructor
     */
    public function __construct(string $prop1, int $prop2)
    {
        $this->prop1 = $prop1;
        $this->prop2 = $prop2;

    }
    /**
     * Les méthodes d'une classe sont composé de la visibilité puis du
     * mot clef function puis du nom de la méthode et de ses arguments
     */
    // public function getProp2() {
    //     return $this->prop2;
    // }

}