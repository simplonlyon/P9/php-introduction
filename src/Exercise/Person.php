<?php

namespace App\Exercise;

class Person {
    public $name;
    public $age;
    public $money;
    /**
     * Avec cette propriété en private, le seul moyen de modifier sa
     * valeur ou d'y accéder sera en passant par des méthodes. Ici
     * addAddress et listAddress nous permettrons respectivement de
     * rajouter de nouvelles adresses et de lister les adresses actuelles
     * de la personne
     */
    private $addresses = [];

    public function __construct(string $name, int $age, float $money) {
        $this->name = $name;
        $this->age = $age;
        $this->money = $money;
    }
    /**
     * Méthode permettant d'ajouter un nouvelle adresse à la personne
     */
    public function addAddress(Address $address):void {
        $this->addresses[] = $address;
        //array_push($this->addresses, $address);
    }
    /**
     * Méthode qui génèrera un ul rempli de li représentant les
     * adresses de la personne
     */
    public function listAddress():string {
        $list = '<ul>';
        foreach ($this->addresses as $key => $address) {
            /* $list .= '<li>';
             $list .= $address->street . ' ';
             $list .= $address->city . ' ';
             $list .= $address->country . ' ';
             $list .= '</li>'; */

            $list .= "<li>$address->street $address->city
             $address->country </li>";

        }

        $list .= '</ul>';
        return $list;
    }
}