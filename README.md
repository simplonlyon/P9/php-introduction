# php-introduction

## Pour lancer le projet
1. Cloner le projet
2. Aller dedans avec le terminal
3. Faire un `composer install`
4. Accéder aux fichiers PHP via localhost

## Exercices

### I. Faire les classes
1. Créer un dossier Exercise dans src
2. Dans ce dossier, créer une première classe Address dans son propre fichier

![Diagramme de la classe Address](./uml/address.jpg)

Ne pas oublier de bien mettre le namespace qu'il faut et de définir les propriétés dans le corps de la classe

3. Créer ensuite une classe Person dans son propre fichier également

![Diagramme de la classe Person](./uml/person.jpg)

* La propriété addresses de la Person sera private, elle ne sera pas en paramètre du __construct() et juste initialisée en tableau vide
* La méthode addAddress va ajouter une nouvelle address au tableau d'addresses de la Person
* La méthode listAddress() va créer une variable dans laquelle vous mettrez un ul puis boucler sur les addresses de la Person (avec un foreach) et à chaque tour de boucle, faire rajouter un li dans la variable (avec un .=) qui contiendra les informations de chaque Address.  En dessous de la boucle, concaténer la fermeture du ul dans la variable. Terminer par un return de la variable
4. Créer à la racine un fichier exercise.php dans lequel vous chargerez le fichier autoload puis vous ferez 2 instances d'Address et une instance de Person sur laquelle vous ferez 2 addAddress() puis un echo de listAddress()

### II. Formulaire Person+Address
1. Créer à la racine un nouveau fichier exercise-form.php
2. Dans ce fichier, faire un formulaire html qui aura les inputs nécessaires pour faire une persone et une address
3. Dans un autre fichier php, ou dans le même, selon votre niveau d'aventurosité, récupérer les informations du formulaire en les filtrant comme il faut
4. Utiliser les informations de ce formulaire pour créer une instance de Person et une instance de Address
5. Ajouter l'Address à la Person puis faire la méthode listAddress de la Person