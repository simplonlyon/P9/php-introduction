<?php


/**
 * Ici, on récupère les informations de la requête POST et on
 * les "filtre" (on échappe/neutralise les caractères nefastes)
 * en utilisant une fonction dédiée.
 * (Il existe plusieurs filtres possibles pour cette fonction listées ici
 * https://secure.php.net/manual/en/filter.filters.sanitize.php)
 */
$data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
/**
 * On obtient un tableau associatif avec comme index les name des différents
 * input du formulaire et comme valeur, les valeurs de ces inputs
 */
echo $data["test"];

//ancienne méthode pas safe
//echo $_POST["test"];