<?php
//Le use est plus ou moins l'équivalent du import ... from ... en JS
//Il se base uniquement sur le namespace
use App\Example\First;

/**
 * On charge le fichier autoload.php qui va permettre de require 
 * automatiquement les classes qu'on utilisera en se basant sur
 * leur namespace
 */
require_once 'vendor/autoload.php';

//Soit avec le nom complet (namespace+classname)
//$instance = new App\Example\First("bloup",20);

//Soit avec le classname + le use en haut du fichier
$instance = new First("bloup", 20);

echo $instance->prop1;

//echo $instance->prop2; //erreur pasque la propriété est private