<?php

use App\Exercise\Address;
use App\Exercise\Person;

require_once "vendor/autoload.php";

$address1 = new Address("251 cours Emile Zola", "Villeurbanne", "France");
$address2 = new Address("221B Baker Street", "London", "England");


$person = new Person("Sherlock Holmes", 21, 200.1);

$person->addAddress($address1);
$person->addAddress($address2);


echo $person->listAddress();
